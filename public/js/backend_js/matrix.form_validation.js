
$(document).ready(function(){
	$("#current_pwd").keyup(function(){ //on click function we have passed current password to /admin/check-pwd URL. //
	var current_pwd = $("#current_pwd").val();
	$.ajax({

	type:'get',
	url:'/admin/update-pwd',
	data:{current_pwd:current_pwd},
	success:function(resp){
		// alert(resp);
		if(resp=="false"){ //response
		$("#chkPwd").html("<font color='red'> Current Password is Incorrect </font>");
		}else if(resp=="true"){
			$("#chkPwd").html("<font color='green'> Current Password is correct </font>");

		}
	},error:function(){
		alert("Error");
	}
		});
	});


	
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	
	$('select').select2();
	
	// Form Validation
    $("#basic_validate").validate({
		rules:{
			required:{
				required:true
			},
			email:{
				required:true,
				email: true
			},
			date:{
				required:true,
				date: true
			},
			url:{
				required:true,
				url: true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});


	// Add Category  Validation

	$("#add_category").validate({
		rules:{
			category_name:{
				required:true
			},
			description:{
				required:true,
			},
			
			url:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	// Edit Category  Validation

	$("#edit_category").validate({
		rules:{
			category_name:{
				required:true
			},
			description:{
				required:true,
			},
			
			url:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	
	// Add Product  Validation

	$("#add_product").validate({
		rules:{
			category_id:{
				required:true
			},
			product_name:{
				required:true
			},
			product_code:{
				required:true,
			},
			product_color:{
				required:true,
			},
			price:{
				required:true,
				number:true
			},
			image:{
				required:true,
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		
	}
});
	// edit Product  Validation

	$("#edit_product").validate({
		rules:{
			category_id:{
				required:true
			},
			product_name:{
				required:true
			},
			product_code:{
				required:true,
			},
			product_color:{
				required:true,
			},
			price:{
				required:true,
				number:true
			},
		
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		
	}
	});


	
	$("#number_validate").validate({
		rules:{
			min:{
				required: true,
				min:10
			},
			max:{
				required:true,
				max:24
			},
			number:{
				required:true,
				number:true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
	
	$("#password_validate").validate({
		rules:{
	
			current_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},

			new_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},
			confirm_pwd:{
				required:true,
				minlength:6,
				maxlength:20,
				equalTo:"#new_pwd"
			}
		},



		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	/*$("#DelCat").click(function(){
		if(confirm('Are you sure you want to delete this Category ?')){
			return true;
		}
		return false;
	});*/

	
	/*$("#DelPro").click(function(){
		if(confirm('Are you sure you want to delete this Product ?')){
			return true;
		}
		return false;
	});*/

	//Delete Category
$(".deleteCategoryRecord").click(function(){
	var id = $(this).attr('rel');
	var deleteFunction =$(this).attr('rel1');
	swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!" ,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes delete it !',
		cancelButtonText: 'No.'
	  },
	  function (){

		window.location.href = "/admin/"+deleteFunction+"/"+id;
	  
	  });
});

//Delete Product
$(".deleteRecord").click(function(){
	var id = $(this).attr('rel');
	var deleteFunction =$(this).attr('rel1');
	swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!" ,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes delete it !',
		cancelButtonText: 'No.'
	  },
	  function (){

		window.location.href = "/admin/"+deleteFunction+"/"+id;
	  
	  });
});

//Delete Coupon
$(".deletecouponRecord").click(function(){
	var id = $(this).attr('rel');
	var deleteFunction =$(this).attr('rel1');
	swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!" ,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes delete it !',
		cancelButtonText: 'No.'
	  },
	  function (){

		window.location.href = "/admin/"+deleteFunction+"/"+id;
	  
	  });
});





//Add Remove Input Fields Dynamically using jQuery


	$(document).ready(function(){
		var maxField = 10; //Input fields increment limitation
		var addButton = $('.add_button'); //Add button selector
		var wrapper = $('.field_wrapper'); //Input field wrapper
		var fieldHTML = '<div class="field_wrapper" style=" margin-left: 180px;"><div><input type="text"  name="sku[]" id="sku" placeholder="SKU" style="width:120px;"/> <input type="text" name="size[]" id="size" placeholder="SIZE" style="width:120px;"/> <input type="text" name="price[]" id="price" placeholder="PRICE" style="width:120px;"/> <input type="text" name="stock[]" id="stock" placeholder="STOCK" style="width:120px;"/> <a href="javascript:void(0);" class="remove_button">Remove</a></div></div>'; //New input field html 
		var x = 1; //Initial field counter is 1
		
		//Once add button is clicked
		$(addButton).click(function(){
			//Check maximum number of input fields
			if(x < maxField){ 
				x++; //Increment field counter
				$(wrapper).append(fieldHTML); //Add field html
			}
		});
		
		//Once remove button is clicked
		$(wrapper).on('click', '.remove_button', function(e){
			e.preventDefault();
			$(this).parent('div').remove(); //Remove field html
			x--; //Decrement field counter
		});
	});

	$(document).ready(function(){
		var maxField = 10; //Input fields increment limitation
		var addButton = $('.add_button_color'); //Add button selector
		var wrapper = $('.field_wrapper'); //Input field wrapper
		var fieldHTML = '<div class="field_wrapper" style=" margin-left: 180px;"><div><input type="text" name="color[]" id="color" placeholder="COLOR" style="width:120px;"/> <a href="javascript:void(0);" class="remove_button_color">Remove</a></div></div>'; //New input field html 
		var x = 1; //Initial field counter is 1
		
		//Once add button is clicked
		$(addButton).click(function(){
			//Check maximum number of input fields
			if(x < maxField){ 
				x++; //Increment field counter
				$(wrapper).append(fieldHTML); //Add field html
			}
		});
		
		//Once remove button is clicked
		$(wrapper).on('click', '.remove_button_color', function(e){
			e.preventDefault();
			$(this).parent('div').remove(); //Remove field html
			x--; //Decrement field counter
		});
	});
//Enable / Disable User

	$(".userStatus").change(function(){
		//alert("test");
		var user_id = $(this).attr('rel');
		//alert(user_id); 
		if($(this).prop("checked") == true){
			//to make status as""enable" = "1"
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  },
				type:'post',
				url:'/admin/update-user-status',
				data:{status:'1' , user_id:user_id},
				success:function(resp){
					//alert(resp);
				},error:function(){
					alert("Error");
				}

			});
		}else{
		   //to make status as""disable" = "0"
		   $.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			type:'post',
			url:'/admin/update-user-status',
			data:{status:'0' , user_id:user_id},
			success:function(resp){
				//alert(resp);
			},error:function(){
				alert("Error");
			}

		});

		}
	});
	
});

$(".photoStatus").change(function(){
	//alert("test");
	var photo_id = $(this).attr('rel');
	//alert(user_id); 
	if($(this).prop("checked") == true){
		//to make status as""enable" = "1"
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  },
			type:'post',
			url:'/admin/update-photo-status',
			data:{status:'1' , photo_id:photo_id},
			success:function(resp){
				//alert(resp);
			},error:function(){
				alert("Error");
			}

		});
	}else{
	   //to make status as""disable" = "0"
	   $.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  },
		type:'post',
		url:'/admin/update-photo-status',
		data:{status:'0' , photo_id:photo_id},
		success:function(resp){
			//alert(resp);
		},error:function(){
			alert("Error");
		}

	});

	}
});
