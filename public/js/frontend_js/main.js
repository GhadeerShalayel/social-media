$().ready(function() {
    // validate signup form on keyup and submit

    $("#registerForm").validate({
        rules: {

            username:{
                required:true,
                remote : '/check-username',
                minlength: 3

            },
            reg_pass: {
                required: true,
                minlength: 5
            },
            conf_pass:{
                required: true,
				equalTo:"#reg_pass"
            },
            reg_email: {
                required: true,
                email: true,
                remote : '/check-email'
            },
            birthday: {
                required: true,
                minlength: 2
            },
            selector: "required"
        },
        messages: {
                
            username: {
                required:"Please enter your Name",
                remote:"User Name  alreay exist !" ,
                minlength: "Your User Name must be at least 3 characters long"


            },
            fullname:{
                required:"Please enter your Full Name",
                minlength: "Your password must be at least 3 characters long",
                //lettersonly :"Please enter letter only in Full Name" 
                //accept:"Please enter letter only in Full Name"


            },
            reg_pass: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            conf_pass:{
				required:"Please provide your Confirm Password",
				minlength: "Your Confirm Password must be equal Password "
			},
            reg_email:{ 

                required: "Please enter your Email",
                email: "Please enter valid Email",
                remote:"Email alreay exist !"     
            }
               
        }   
    });
    // Validate Gender selctor on keyup and submit
    


    $('#registerForm').submit(function(e) {
        e.preventDefault(); // Stop your form from submitting.
        if ('0' === $('#gender').val()) {
          $('#errorInPopup').html('Please select your gender');
          return false;
        } else {
          this.submit();
        }
      });

    // validate signup form on keyup and submit

    $("#DatingForm").validate({
        rules: {

            fullname:{
                required:true,
                minlength: 3

            },
            height: {
                required: true,
            },
            weight:{
                required: true,
            },
            city: {
                required: true,
            },
            dob: {
                required: true,
            },
            marital_status:{
                required: true,
            },
            marital_status:{
                required: true,
            },  
            body_type:{
                required: true,
            }, 
            complexion:{
                required: true,
            },
            counrty:{
                required: true,
            },  
            language:{
                required: true,
            },
            myself:{
                required: true,
                minlength:10,
            },
            partner:{
                required: true,
                minlength:10,

            },
            selector: "required"
        },
        messages: {
            fullname:{
                required:"Please enter your Full Name",
                minlength: "Your Name must be at least 3 characters long",
                //lettersonly :"Please enter letter only in Full Name" 
                //accept:"Please enter letter only in Full Name"


            },
            height: {
                required:"Please enter your height",
            },
            weight:{
                required:"Please enter your weight",
			},
            city:{ 
                required:"Please enter your city",
            },
            dob:{ 
                required: "Please enter your Date of Birth",     
            }
               
        }   
    });
    
  
	// Validate Gender selctor on keyup and submit


    $('#reg_pass').keyup(function(e) {
        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
        var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
        var enoughRegex = new RegExp("(?=.{6,}).*", "g");
        if (false == enoughRegex.test($(this).val())) {
                /*$('#passstrength').html('More Characters');*/
        } else if (strongRegex.test($(this).val())) {
                $('#passstrength').className = 'ok';
                $('#passstrength').html("<font color='green'> Strong!  </font>");
        } else if (mediumRegex.test($(this).val())) {
                $('#passstrength').className = 'alert';
                $('#passstrength').html("<font color='orange'> Medium!  </font>");
        } else {
                $('#passstrength').className = 'error';
                $('#passstrength').html("<font color='red'> Weak!  </font>");
        }
        return true;
    });
    

});

// MDB Lightbox Init
$(function () {
    $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
});

// SweetAlert Delete Script
$(".deleteAction").click(function(){ 
    var action = $(this).attr('rel');
    var deleteRoute = $(this).attr('rel1');
    swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
              reverseButtons: true
    },
    function(){
        window.location.href="/"+deleteRoute+"/"+action;
    });
});

$(".updateResponse").click(function(){
    var response_id = $(this).attr('rel');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        type:'post',
        url:'/update-response',
        data:{response_id:response_id},
        success:function(resp){
           // alert(resp);
            $(".rel1-"+response_id).addClass('seenResponse');
            $(".newResponsesCount").html(resp);
        },error:function(){
            //alert("Error");
        }
    })
});



$(".deleteMessage").click(function(){ 
    var action = $(this).attr('rel');
    var deleteRoute = $(this).attr('rel1');
    swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
              reverseButtons: true
    },
    function(){
        window.location.href="/"+deleteRoute+"/"+action;
    });
});

$(document).ready(function() {
    $('.galleryes').magnificPopup(
        {
            delegate:'a',
            type:'image',
            gallery: {
                // options for gallery
                enabled: true
              }
    });
  });

  $(document).ready(function() {
    $('#example').DataTable();
    responsive: true

} );



