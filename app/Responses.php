<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Responses extends Model
{
    public static function newResponsesCount (){
        $recever_id = Auth::user()->id;
        $ResponeseCount = Responses::where(['receiver_id'=>$recever_id , 'seen'=>0])->count();
        return $ResponeseCount;
    }
}
