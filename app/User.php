<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function details(){
        return $this->hasOne('App\UsersDetail', 'user_id');

    }
    public function photos(){
        return $this->hasMany('App\UsersPhoto', 'user_id');
    }

    public static function datingProfileExists($user_id){
        $DatingProfileCount = UsersDetail::select('user_id','status')->where(['user_id'=>$user_id,'status'=>'1'])->count();
        return $DatingProfileCount;
    }

    public static function datingProfileDetails($user_id){
        $datingProfile = UsersDetail::where('user_id',$user_id)->first();
        return $datingProfile;
    }
    public static function getImage($user_id){
        $getImage = UsersPhoto::where('user_id',$user_id)->first();
        return $getImage->photo;
    }
    public static function getName($user_id){
        $getName = User::select('user_name')->where('id',$user_id)->first();
        return $getName->user_name;
    }
    
    public static function getCity($user_id){
        $getCity = UsersDetail::select('city')->where('user_id',$user_id)->first();
        return $getCity->city;
    }

    public static function getUserId($user_name){
        $getId = User::select('id')->where('user_name',$user_name)->first();
        return $getId->id;
    }


}
