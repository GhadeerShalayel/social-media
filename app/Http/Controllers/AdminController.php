<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Session;
use App\User;
use App\Admin;
use Illuminate\Support\Facades\Hash; // this to check Hash Password


class AdminController extends Controller
{

            //function login start 

    public function login(Request $request){
         /* if condition with request -> is method ('post') to check if login form submited */
        if($request -> isMethod('POST')){
            $data = $request ->input();
            $adminCount = Admin::where(['user_name'=>$data['user_name'],'password'=>md5($data['password']),'status'=>'1'])->count();
         /*  use auth to check if admin email and pass are correct and user acutlly  is admin */ 
            if($adminCount > 0){
                Session::put('adminSession',$data['user_name']);
         //  Session::put('adminSession',$data['email']); //We have added adminSession at the time of Successful login
           // We have used Session::put method for creating the Session variable adminSession and assigned username to it.
         //Then we have compared this in dashboard function to check if adminSession exists or not.
         // If exists then would execute dashboard tasks otherwise would redirect back to admin login.
                return redirect('/admin/dashboard');
            }else{
                //echo "Failed" ; die ;
                return redirect('/admin')->with('flash_message_error','Invalid User name  Or Password');
            }
        }
        return view('admin.admin_login');
    }        //function login end 





        //function dashboard start 
        public function dashboard(){

           /* if(Session::has('adminSession')){
               //when we tried to access dashboard page without login.. we couldn't able to access dashboard page directly.
                //all dashboard tasks
            }else{
                return redirect('/admin')->with('flash_message_error','Please Login to access');
            }*/

          return view('admin.dashboard');

        } //function dashboard end



        public function settings(){
            $adminDetails = Admin::where(['user_name'=>Session::get('adminSession')])->first();
            /*$adminDetails = json_decode(json_encode($adminDetails));
            echo "<pre>" ; print_r($adminDetails) ; die;*/

            return view('admin.settings')->with(compact('adminDetails'));
        }




        public function chkPassword(Request $request){ /*Now we have created checkPassword function 
            where we have checked current password entered by the user is correct or not.
            We have returned true or false message to Ajax to display the message on our form.*/
            
                $data = $request->all();
                $current_password = $data['current_pwd'];
                //$user = Admin::admins()->user_name;
                $check_password = Admin::where(['user_name'=>Session::get('adminSession')])->first();
                $adminCount = Admin::where(['user_name'=>Session::get('adminSession'),'password'=>md5($current_password)])->count();
                if($adminCount == 1){
                    echo "true"; die;
                }else {
                    echo "false"; die;
                }
            }


      /*in which we have again checked current password and if correct then we updated it 
            and return to settings page with success message otherwise we have displayed error message.*/


            public function updatePassword(Request $request){
                if($request->isMethod('post')){
                    $data = $request->all();
                    //$user = Auth::user()->email;
                    $current_password = $data['current_pwd'];
                    $adminCount = Admin::where(['user_name'=>Session::get('adminSession'),'password'=>md5($current_password)])->count();
                    if($adminCount == 1){
                        $password = md5($data['new_pwd']);
                        Admin::where(['user_name'=>Session::get('adminSession')])->update(['password'=>$password]);
                        return redirect('/admin/settings')->with('flash_message_success','Password updated Successfully!');
                    }else {
                        return redirect('/admin/settings')->with('flash_message_error','Incorrect Current Password!');
                    }
                }
            }
        




        public function logout(){

            Session::flush(); // To clear of all session 

            return redirect('/admin')->with('flash_message_success','Logout Successfully');

        }


    }
    

