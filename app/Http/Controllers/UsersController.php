<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use Image;
use App\User;
use App\UsersDetail;
Use App\Country;
Use App\Language;
Use App\Hobby;
Use App\Responses;
Use App\Frind;
Use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\UsersPhoto;

class UsersController extends Controller
{
//Register Function start
    public function register(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>" ; print_r($data) ; die;
            //It will return back to register  if captcha not selected 
            $this->validate($request , ['g-recaptcha-response' =>'required|captcha', ]);
            $userCount = User::where('email',$data['reg_email'])->count();
            if( $userCount >0){
                return redirect()->back()->with('flash_message_error','Email already exists!');
            }else{
            
                $user = new User;
                $user->user_name = $data['username'];
                //$user->birthday = $data['birthday'];
                $user->email = $data['reg_email'];
                $user->password = bcrypt($data['reg_pass']);
                $user->gender = $data['gender'];

                $user->save();
                if(Auth::attempt(['user_name'=>$data['username'] , 'password'=>$data['reg_pass']])){
                        //echo "success" ; die;
                        Session::put('frontSession',$data['username']);
                        return redirect('/step/2');
                }
            }
        }
        return view('user.register');
    }
//Register Function end

//check Email Function start

    public function checkEmail(Request $request){
        $data = $request->all();
        $userCount = User::where('email' , $data['reg_email'])->count();
        if($userCount > 0){
            echo "false";
        }else{
            echo "true"; die;
        }
    }
//check Email Function end

//check User name  Function start

    public function checkUsername(Request $request){
        $data = $request->all();
        $userCount = User::where('user_name' , $data['username'])->count();
        if($userCount > 0){
            echo "false";
        }else{
            echo "true"; die;
        }
    }
//check User name  Function end

//login Function start

    public function login(Request $request){
        if($request->isMethod('post')){
            $data= $request->all();
           // echo "<pre>" ; print_r($data) ; die;
            if(Auth::attempt(['user_name'=>$data['username'] , 'password'=>$data['password']])){
                //echo "success" ; die;
                Session::put('frontSession',$data['username']);
                return redirect('/step/2');
            }else{
               // echo "field";die;
               return redirect()->back()->with('flash_message_error','Invalid User name  Or Password');
            }
        }
        return view('user.login');
    }
//login Function end

//step2 Function start

    public function step2(Request $request){
        $user_id = Auth::User()['id'];
//Check if dating profile exist
        $userProfileCount = UsersDetail::where(['user_id'=>$user_id , 'status'=>0])->count();
        if($userProfileCount>0){
            return redirect('/review');
        }

        //echo "<pre>" ; print_r($user_id) ; die;
        if($request->isMethod('post')){
            $data = $request->all();
            if(empty($data['user_id'])){
                $UserDetail =  new UsersDetail;
                $UserDetail->user_id = $user_id;
            }else{
                $UserDetail = UsersDetail::where('user_id',$user_id)->first();
                $UserDetail->status = 0;
            }
   
            $UserDetail->fullname = $data['fullname'];
            $UserDetail->user_name = $data['user_name'];
            $UserDetail->dob = $data['dob'];
            $UserDetail->height = $data['height'];
            $UserDetail->weight = $data['weight'];
            $UserDetail->marital_status = $data['marital_status'];
            $UserDetail->body_type = $data['body_type'];
            $UserDetail->complexion = $data['complexion'];
            $UserDetail->counrty = $data['counrty'];
            $UserDetail->city = $data['city'];
            $UserDetail->state = $data['state'];
            $UserDetail->language = $data['language'];
            $UserDetail->hobby = $data['hobby'];
            $UserDetail->education = $data['education'];
            $UserDetail->occupation = $data['occupation'];
            $UserDetail->income = $data['income'];
            $UserDetail->myself = $data['myself'];
            $UserDetail->partner = $data['partner'];

            

            $UserDetail->save();
            return redirect('/review');

        }

        $User = User::find($user_id);
        //Get all country
        $countries = Country::get();

        //Get all language
        $languges = Language::orderby('name','ASC')->get();

        //Get all Hobby
        $hobbies = Hobby::orderby('title','ASC')->get();
        $UserDetail = UsersDetail::where('user_id',$user_id)->first();
        $UserDetail = json_decode(json_encode($UserDetail));
       // echo "<pre>" ; print_r($UserDetail) ; die;
        return view('user.step2')->with(compact('User','countries','languges','hobbies','UserDetail'));

    }
//step2 Function end

//Review function start
public function review(){
    $user_id = Auth::User()['id'];
    $userStatus = UsersDetail::select('status')->where('user_id',$user_id)->first();
    if($userStatus->status == 1){
        return redirect('/step/2');
    }else{
        return view('user.review');
    }
  
}
//Review function end


//View User Function start
    public function viewUsers(){
        $users = User::with('details')->with('photos')->get();
        $users = json_decode(json_encode($users),true);
        //echo "<pre>" ; print_r($users) ; die;
        return view('admin.users.view_users')->with(compact('users'));
    }
//View function end

//update User Status User Function start

    public function updateUserStatus(Request $request){
        $data = $request->all();
        UsersDetail::where('user_id',$data['user_id'])->update(['status'=>$data['status']]);
    }

    public function updatePhotoStatus(Request $request){
        $data = $request->all();
        UsersPhoto::where('id',$data['photo_id'])->update(['status'=>$data['status']]);
    }


//update User Status User Function end
    public function step3(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            if($request->hasFile('photo')){
                $files = $request->file('photo');
                foreach($files as $file){

                    // Add photo at photos Folder

                    // Get photo extension
                    $extension = $file->getClientOriginalExtension();
                    // Give Random name to image and add its extension
                    $fileName = rand(111,99999).'.'.$extension;
                    // Set Image Path 
                    $image_path = 'images/frontend_images/photos/'.$fileName;
                    // Intervention Code for uploading Image
                    Image::make($file)->resize(500,500)->save($image_path);

                    // Add photo at users_photos table

                    $photo = new UsersPhoto;
                    $photo->photo = $fileName;
                    $photo->user_id = Auth::User()['id'];
                    $photo->user_name = Auth::User()['user_name'];
                    $photo->save();
                }
            }
            return redirect('/step/3')->with('flash_message_success','Your photo(s) has been added successfully.');
        }
        $user_id = Auth::User()['id'];
        $user_photos = UsersPhoto::where('user_id',$user_id)->orderBy('id','desc')->get();
        return view('user.step3')->with(compact('user_photos'));
    }

    public function deletePhoto($photo){
       // echo $photo ; 
        $user_id = Auth::User()['id']; 
        UsersPhoto::where(['user_id'=>$user_id , 'photo'=>$photo])->delete();
        // Delete from photos folder :- with PHP unlink function
        //unlink('images/frontend_images/photos/'.$photo);

        //Delete from photos folder :- with Laravel File:delete function
        File::delete('images/frontend_images/photos/'.$photo);

        return redirect()->back()->with('flash_message_success','Photo has been delete successfully');

    }
    
    public function defaultPhoto($photo){
        // echo $photo ; 
         $user_id = Auth::User()['id']; 
         //make all photo in non defult
         UsersPhoto::where('user_id', $user_id )->update(['default_photo'=>"No"]);
         //Make selected photo is defult
         UsersPhoto::where(['user_id'=>$user_id , 'photo'=>$photo])->update(['default_photo'=>"Yes"]);

         return redirect()->back()->with('flash_message_success','Photo has been Updated successfully');
 
     }

    public function viewProfile($user_name){
        $user_count = User::where('user_name',$user_name)->count();
        //echo "<pre>" ; print_r($user_count); die;
        if($user_count == 0){
            abort(404);
        }
        if(Auth::check()){
            $user_id = Auth::user()->id;
            $frind_id = user::getUserId($user_name);
            $frindCount = Frind::where(['user_id'=> $user_id , 'friend_id'=>$frind_id])->count();

            if($frindCount > 0){
                $frindDetails = Frind::where(['user_id'=> $user_id , 'friend_id'=>$frind_id])->first();
                //echo "<pre>" ; print_r($frindDetails); die;
                if($frindDetails->accept == 1){
                    $frindRequest = "Unfrind";
                }else{
                    $frindRequest = "Request Sent";

                }

            }else{
                $frindRequest = "Add Frind";
            }

        }
        $userDetails = User::with('details')->with('photos')->where('user_name',$user_name)->first();
        $userDetails = json_decode(json_encode($userDetails),true);

        //echo "<pre>" ; print_r($userDetails); die;
       return view('user.profile')->with(compact('userDetails','frindRequest'));
    }
    
    public function viewProfilePhoto($user_name){
        $userDetails = User::with('details')->with('photos')->where('user_name',$user_name)->first();
        $user_photos = UsersPhoto::where('user_name',$user_name)->orderBy('id','desc')->get();

        //echo "<pre>" ; print_r($userPhotos); die;
       return view('user.ProfilePhoto')->with(compact('userDetails','user_photos'));
    }

    
    public function searchProfile(Request $request){
        $countries = Country::get();

        if($request->isMethod('post')){
            $data = $request->all();

            //echo "<pre>" ; print_r($data) ; die;
            $search_user = User::with('details')->with('photos')->
            join('users_details','users_details.user_id' , '=' , 'users.id')->
            where('users.gender',$data['gender']);
            if(!empty($data['location'])){
                $search_user = $search_user->where('users_details.counrty',$data['location']);
            }                                                                                   
            $search_user = $search_user->orderBy('user_id','Desc')->get();

            $search_user = json_decode(json_encode($search_user),true);
            $recent_users = User::with('details')->with('photos')->orderBy('id','Desc')->get();
            $minAge = $data['minAge'];
            $maxAge = $data['maxAge'];
          //echo "<pre>" ; print_r($search_user) ; die; 
        }
        return view('user.search')->with(compact('search_user','countries','recent_users','minAge','maxAge'));
    }


    public function member(){
        $recent_users = User::with('details')->with('photos')->orderBy('id','Desc')->get();
        $recent_users = json_decode(json_encode($recent_users),true);

        $countries = Country::get();
        //echo "<pre>" ; print_r($recent_users) ; die;
        return view('user.member')->with(compact('recent_users','countries'));
    }

    public function contactProfile(Request $request){
        if($request->isMethod('post')){
            $data=$request->all();
            //echo "<pre>" ; print_r($data) ; die;
            $response = new Responses;
            $response->sender_id = $data['sender_id'];
            $response->receiver_id = $data['receiver_id'];
            $response->message = $data['message'];
            $response->save();
            return redirect()->back()->with('flash_message_success','Your message has been sent to this  profile.');

        }
    }

    public function responses(){
        //بدي اعرف مين بعتلي ف الريسيفر هان انا ف من الجدول بدي اشوف وقتيش انا تلقيت رسالة كيف ؟ بدي اقارن الاي دي تبعي مع الاي دي تبع الريسيفر
        $receiver_id = Auth::user()['id'];
        $responses = Responses::where('receiver_id',$receiver_id)->orderBy('id','desc')->get();
        //echo "<pre>" ; print_r($responses) ; die;

        return view('user.responses')->with(compact('responses'));
    }

    public function sentMessages(){
        $sender_id = Auth::user()['id'];
        $senders = Responses::where('sender_id',$sender_id)->orderBy('id','Desc')->get();
        return view('user.sent_messages')->with(compact('senders'));
    }


    public function deleteResponse($id){
        Responses::where('id',$id)->delete();
        return redirect()->back()->with('flash_message_success','The message has been deleted .');
    }

    public function updateResponse(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            Responses::where('id',$data['response_id'])->update(['seen' => 1]);
            echo Responses::newResponsesCount(); 
        }
    }

    public function addFriend($user_name){

        $user_id = Auth::user()->id;
        $frind_id = User::getUserId($user_name);
        $frind = new Frind;
        $frind->user_id = $user_id ;
        $frind->friend_id = $frind_id;
        $frind->save();

        return redirect()->back();
    }


    public function removeFriend($user_name){
        $user_id = Auth::user()->id;
        $frind_id = User::getUserId($user_name);

        Frind::where(['user_id'=>$user_id , 'friend_id'=>$frind_id])->delete();
        return redirect()->back();
    }

    public function friendsRequests(){
        $user_id = Auth::user()->id;
        $frindRequest = Frind::where(['friend_id'=>$user_id , 'accept'=>0])->get();
        
        return view('user.friends_requests')->with(compact('frindRequest'));

    }

    public function acceptfriendRequest($sender_id){
        $receiver_id = Auth::user()->id;
        Frind::where(['user_id'=>$sender_id ,'friend_id'=>$receiver_id ])->update(['accept'=>1]);
        return redirect()->back();
    }

    public function rejectfriendRequest($sender_id){
        $receiver_id = Auth::user()->id;
        Frind::where(['user_id'=>$sender_id ,'friend_id'=>$receiver_id ])->delete();
        return redirect()->back();
    }

    public function friends (){
        $user_id = Auth::user()->id;
        $frindsCount = Frind::where(['friend_id'=>$user_id , 'accept'=>1])->count();
        $frinds = Frind::where(['user_id'=>$user_id , 'accept'=>1])->simplePaginate(50);
       // echo "<pre>" ; print_r($frinds) ; die;

        if($frindsCount > 0){
            //هما الي بعتولي طلب 
            $frinds = Frind::where(['friend_id'=>$user_id , 'accept'=>1])->simplePaginate(50);
        }else{
            //هان انا الي بعتت الطلب للناس
            $frinds = Frind::where(['user_id'=>$user_id , 'accept'=>1])->simplePaginate(50);
        }

        return view('user.friends')->with(compact('frinds'));

    }


//logout Function start
    public function logout(){
        Auth::logout();
        Session::forget('frontSession');

        return redirect('/');

    }
//logout Function end


}

