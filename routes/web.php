<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Route::get('/', function () {
    return view('welcome');
});*/
/*Route::get('/', function () {
    return view('coming-soon');
});*/

//Home Page

Route::get('/','IndexController@index');

Route::match(['get','post'] , '/admin' , 'AdminController@login'); /*Sometimes you may need to register a route that responds to multiple HTTP verbs. You may do so using the match method. Or, you may even register a route that responds to all HTTP verbs using the any method:*/
Auth::routes();
/*Sometimes you may need to register a route that responds to multiple HTTP verbs.
 You may do so using the match method. Or, you may even register a route that responds to all HTTP verbs using the any method:*/

//User Register Route
Route::any('/register','UsersController@register');
Route::any('/check-email','UsersController@checkEmail');
Route::any('/check-username','UsersController@checkUsername');
Route::any('/login','UsersController@login');
Route::get('/logout','UsersController@logout'); 
Route::get('/members','UsersController@member');
Route::any('/search','UsersController@searchProfile');
Route::get('/profile/{usename}','UsersController@viewProfile');
Route::get('/profile/photo/{usename}','UsersController@viewProfilePhoto');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/logout','AdminController@logout');


Route::group(['middleware'=>['frontlogin']] , function(){
    Route::any('/step/2','UsersController@step2');
    Route::any('/step/3','UsersController@step3');
    Route::get('/review','UsersController@review');
    Route::get('/delete-photo/{photo}','UsersController@deletePhoto'); 
    //Get route for setting default photo 
    Route::get('/default-photo/{photo}','UsersController@defaultPhoto');
    Route::match(['get','post'],'/contact/{username}','UsersController@contactProfile');
    Route::get('/responses','UsersController@responses');
    Route::get('/sent-messages','UsersController@sentMessages');
    Route::get('/delete-response/{id}','UsersController@deleteResponse');
    Route::post('/update-response','UsersController@updateResponse');
    Route::match(['get','post'],'/add-friend/{username}','UsersController@addFriend');
    Route::match(['get','post'],'/remove-friend/{username}','UsersController@removeFriend');
    Route::get('/friends-requests','UsersController@friendsRequests');
    Route::get('/accept-friend-request/{id}','UsersController@acceptfriendRequest');
    Route::get('/reject-friend-request/{id}','UsersController@rejectfriendRequest');
    Route::get('/friends','UsersController@friends');


});
Route::group(['middleware' => ['adminLogin'] ] , function(){

    //admin routes
    Route::get('/admin/dashboard','AdminController@dashboard'); 
    Route::get('/admin/settings','AdminController@settings'); 
    Route::get('/admin/check-pwd','AdminController@chkPassword'); //Now we have created route for checking current password that we have passed via Ajax URL 
    Route::match(['get','post'] , '/admin/update-pwd' , 'AdminController@updatePassword');//added both GET/POST method in route 
    Route::get('/admin/view-users','UsersController@viewUsers');
    Route::post('/admin/update-user-status','UsersController@updateUserStatus');
    Route::post('/admin/update-photo-status','UsersController@updatePhotoStatus');



} );
