<?php use App\User ;
      use App\Responses;
?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<style>
.seenResponse{
    font-weight: normal !important;
}
</style>
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Dating Profile<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">Dating Profile</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
      <div class="register_form_inner zoom-anim-dialog " id="register_form">
         <div class="row">
            @if(Session::has('flash_message_error'))    
            <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_error') !!} </strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))  
            <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_success') !!} </strong>
            </div>
            @endif
            <div class="registration_form_s ">
               <h4>The messages I received </h4>
               <h5 style="padding-bottom: 15px; font-weight:bold;"> UnRead Messages <span class="newResponsesCount" >{{Responses::newResponsesCount()}} </span> </h5>
               <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Message</th>
                        <th>Date/Time</th>
                        <th>Actions</th>

                     </tr>
                  </thead>
                  <tbody>
                    @foreach($responses as $response)
                        <?php $sender_name = User::getName($response->sender_id);
                              $sender_city = User::getCity($response->sender_id); 
                              $encoded_message = encrypt($response->message); 
                            
                        ?>
                     <tr>
                        @if($response->seen == 0) 
                           <?php $response_style= 'style=font-weight:bold;' ;?>
                        @else
                           <?php $response_style= 'style=font-weight:normal;' ;?>
                        @endif
                        <td class="rel1-{{ $response->id }}" {{$response_style}} > <span class="newResponsesCount"></span>{{$sender_name}}</td>
                        <td class="rel1-{{$response->id}}" {{$response_style}} >{{substr($response->message, 0, 15)}}<a href="" data-toggle="modal" data-target="#responsesDetails{{$response->id}}" title="view Details" >...</a></td>
                        <td class="rel1-{{$response->id}}" {{$response_style}} >{{$response->created_at}}</td>
                        <td  style="text-align:center;">
                           <a class="updateResponse " rel="{{$response->id}}" title=" view details " data-toggle="modal" data-target="#responsesDetails{{$response->id}}" style="color:black;" href=""><i class="fa fa-file-text-o FonthoverGreen" aria-hidden="true"></i></a>
                              <div class="modal fade" id="responsesDetails{{$response->id}}" tabindex="-1" role="dialog" aria-labelledby="responsesDetails{{$response->id}}" aria-hidden="true">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button data-dismiss="modal" class="close" type="button">×</button>
                                          <h3>Messages Details</h3>
                                       </div>
                                       <div class="modal-body">
                                          <p>{{$response->message}}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                     
                           <a style="color:black;" href="#small-dialog" class="popup-with-zoom-anim"><i class="fa fa-reply FonthoverOrange" aria-hidden="true"style="margin-right: 10px;"></i></a>
                           <div class="login_form_inner zoom-anim-dialog mfp-hide" id="small-dialog">
                                    <form id ="messageForm "action="{{url('/contact/'.$sender_name)}}" method="post">
                                          {{csrf_field()}}
                                          <div class="form-group shadow-textarea"> 
                                             <label for="exampleFormControlTextarea6"></label>
                                             <textarea  class="form-control z-depth-1" id="exampleFormControlTextarea6" id="message" name="message" rows="3" required placeholder="Write Your Message here..."></textarea>
                                             <input type="hidden" name="receiver_id" value="{{$response->sender_id}}">
                                             <input type="hidden" name="sender_id" value="{{$response->receiver_id}}">
                              
                                          </div> 
                                          <div class="login_btn_area">
                                             <button type="submit" value="LogIn" class="btn form-control login_btn" style="width: 90px; text-align: center; " >Send</button>
                                          </div>
                                    </form>
                                    <img class="mfp-close" src="img/close-btn.png" alt="">
                            </div>

                           <a style="color:black;" rel="{{$response->id}}"  rel1="delete-response" class="deleteMessage"><i class="fa fa-trash-o FonthoverRed" aria-hidden="true"></i></a>

                        </td>

                     </tr>
                     @endforeach
                  </tbody>
               </table>                                                         
            </div>
         </div>
      </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
