@extends('layouts.frontlayouts.front_design')
@section('content')

       <!--================Banner Area =================-->
       <section class="banner_area profile_banner">
            <div class="profiles_inners">
                <div class="container">
                    <div class="profile_content">
                        <div class="user_img">
                                @foreach($userDetails['photos'] as $key => $photo)
                                    @if($photo['default_photo'] == "Yes")
                                        <?php $user_photo = $userDetails['photos'][$key]['photo'] ;?>
                                    @else
                                        <?php $user_photo = $userDetails['photos'][0]['photo'] ; ?>
                                    @endif
                                @endforeach

                                @if(!empty($user_photo))
                                <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/'.$user_photo)}}" alt="">
                                @else  
                                    @if($userDetails['gender']=="male")
                                        <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/male.png')}}" alt="">
                                    @else
                                        <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/female.jpg')}}" alt="">

                                    @endif

                                @endif
                        </div>
                        <div class="user_name">
                            <h3>{{ $userDetails['user_name']}}</h3>
                            <h4> <?php echo $diff = date('Y') - date('Y',strtotime($userDetails['details']['dob']));?> years old</h4>
                            <ul>
                                <li><a>{{$userDetails['details']['counrty']}}, {{$userDetails['details']['city']}}</a></li>
                            
                            </ul>
                        </div>
                        <div class="right_side_content">
                             <ul class="nav navbar-nav">
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Another action</a></li>
                                  </ul>
                                </li>
                              </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        
        <!--================Blog grid Area =================-->
        <section class="blog_grid_area">
            <div class="container">
                <div class="row galleryes" >
                        <div class="s_title">
                            <h4 style="margin-left: 5px;">My Photos</h4>
                                <img src="{{ asset ('images/frontend_images/w-title-b.png')}}" alt="">
                        </div>
                        @foreach($user_photos as $photo)
                        @if($photo['status']==1)
                        <div class="col-md-3 col-sm-6">
                            <div class="product_item">
                                <div class="product_img gellary">
                                    <a  href="{{ url ('images/frontend_images/photos/'.$photo['photo']) }}" target="_blank" ><img src="{{ asset ('images/frontend_images/photos/'.$photo['photo']) }}" alt=""> </a>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                </div>

            </div>
        </section>
        <!--================End Blog grid Area =================-->
        
    
@endsection