<?php 
namespace App\Http\Controllers;
use Auth;
use App\User ;

?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area banner_area2">
   <div class="container">
      <div class="banner_content">
         <h3 title="Members"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Members<img class="right_img" src="img/banner/t-right-img.png" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="">Community</a>
         <a href="blog.html">Members</a>
         <div class="advanced_search">
            <form action="{{url('/search')}}" method="post">
               {{@csrf_field()}} 
            <div class="search_inner">
               <div class="search_item">
                   <?php    $gender = Auth::User()['gender'];?>
                   <input type="hidden" value="{{ $gender }}">
                  <h5>Seeking a</h5>
                  <select class="selectpicker" name="gender">
                     <option value="female">Woman</option>
                     <option value="male">Man</option>
                  </select>
               </div>
               <div class="search_item">
                  <h5>From</h5>
                  <select class="selectpicker" name="minAge">
                    <?php   $minacount = 16 ;
                    while($minacount <= 99 ){ ?>
                     <option value="{{$minacount}}">{{$minacount}}</option>
                     <?php $minacount = $minacount + 1 ; } ?>
                  </select>
               </div>
               <div class="search_item">
                  <h5>To</h5>
                  <select class="selectpicker" name="maxAge">
                    <?php   $maxacount = 16 ;
                    while($maxacount <= 99 ){ ?>
                     <option value="{{$maxacount}}">{{$maxacount}}</option>
                     <?php $maxacount = $maxacount + 1 ; } ?>

                  </select>
               </div>
               <div class="search_item">
                  <h5>Location</h5>
                  <select class="selectpicker" name="location">
                  <option value ="" selected>AnyWhere</option>
                        @foreach($countries as $counrty)
                        <option value="{{$counrty->country_name}}" @if($counrty->country_name == "palestine") selected @endif  > {{$counrty->country_name}}</option>
                        @endforeach
                  </select>
               </div>
               <div class="search_item">
                  <button type="submit" style="width: 130px;" value="LogIn" class="btn form-control login_btn">Search</button>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Active Memebers Area =================-->
<section class="actives_members">
   <div class="container">
      <div class="welcome_title">
      @if(!empty($search_user))

         <h3>Result Search</h3>
         <img src="{{ asset ('images/frontend_images/w-title-b.png')}}" alt="">
      </div>
      <div class="row">
          @foreach($search_user as $user)
          @if(!empty($user['details']) && $user['details']['status']==1)
          <?php $age = date('Y') - date('Y' , strtotime($user['details']['dob']) ) ; ?>
          @if($age >= $minAge && $age <= $maxAge )
         <div class="col-sm-2 col-xs-6" style="margin-bottom: 15px;">
            <div class="active_mem_item">
               <ul class="nav navbar-nav">
                  <li class="dropdown tool_hover">
                     @if(!empty($user['photos']) & $user['photos']['status']==1)
                           @foreach($user['photos'] as $key => $photo)
                              @if($photo['default_photo'] == "Yes")
                                 <?php $user_photo = $user['photos'][$key]['photo'] ;?>
                              @else
                                 <?php $user_photo = $user['photos'][0]['photo'] ; ?>
                              @endif
                           @endforeach
                           @if(!empty($user_photo))
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img  class="img-circle"  src="{{ asset ('images/frontend_images/photos/'.$user_photo)}}" alt=""></a>
                           @endif 
                     @else
                           @if($user['gender']=="male")
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">   <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/male.png')}}" alt=""></a>
                           @else
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">   <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/female.jpg')}}" alt=""></a>

                           @endif

                     @endif
                    
                  </li>
               </ul>
               <h4>{{$user['details']['user_name']}}</h4>
               <h5>{{$age}} years old</h5>
            </div>
         </div>
         @endif
         @endif
         @endforeach
         @endif

      </div>
   </div>
</section>

@endsection
