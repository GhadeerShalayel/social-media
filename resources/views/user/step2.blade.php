<?php use App\User;
     $DatingProfileCount = User::datingProfileExists(Auth::User()['id']);
     if($DatingProfileCount ==1 ){
         $datingProfileText = "My Dating Profile";
     }else{
        $datingProfileText = "Add Dating Profile";
     }
     $datingProfile = User::datingProfileDetails(Auth::User()['id']);
  //die;
  ?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Dating Profile<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">Dating Profile</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
      <div class="register_form_inner zoom-anim-dialog " id="register_form">
         <div class="row">
            @if(Session::has('flash_message_error'))    
            <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_error') !!} </strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))  
            <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_success') !!} </strong>
            </div>
            @endif
            <div class="registration_form_s ">
               <h4>{{$datingProfileText}} </h4>
               <form method="post" name ="DatingForm" action="{{url('/step/2')}}" id="DatingForm">
                  {{csrf_field()}} 
                  @if(!empty($datingProfile->user_id))
                  <input type="hidden" name = "user_id" value="{{$datingProfile->user_id}}">
                  @endif
                  <div class="col-md-6">
                    <h3 style="text-align: center; margin-bottom: 12px;margin-top: 12px;"> Personal Information </h3>
                    <div class="form-group">
                        <lable> User Name </lable>
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Full Name"  value="{{ $User['user_name'] }}"  >
                     </div>
                     <div class="form-group">
                        <lable> Full Name </lable>
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" @if(!empty($datingProfile['fullname'])) value="{{ $datingProfile['fullname'] }}" @endif >
                     </div>
                     <div class="form-group">
                        <lable>Your Hight</lable>
                        <input type="text" class="form-control" id="Height" name="height" placeholder="Your Hight " @if(!empty($datingProfile['height'])) value=" {{$datingProfile['height']}}"  @endif>
                     </div>
                     <div class="form-group">
                        <lable>Your Weight</lable>
                        <input type="text" class="form-control" id="weight" name="weight" placeholder="Your Weight" @if(!empty($datingProfile['height'])) value=" {{$datingProfile['weight']}}" @endif>
                     </div>
                     <div class="form-group">
                        <lable>Your City</lable>
                        <input type="text" class="form-control" id="city" name="city" placeholder="City :" @if(!empty($datingProfile['height'])) value=" {{$datingProfile['city']}}"  @endif>
                     </div>
                     <div class="form-group">
                        <lable>Your State</lable>
                        <input type="text" class="form-control" id="state" name="state" placeholder="State :" @if(!empty($datingProfile['height'])) value=" {{$datingProfile['state']}}"  @endif>
                     </div>
                     <div class=" form-group" style="margin-bottom: 20px;">
                        <lable>Your DOB</lable>
                        <input type="text" autocomplete="off" class="form-control" id="dob" name="dob" placeholder="Date Of Birth" @if(!empty($datingProfile['dob'])) value="{{ $datingProfile['dob'] }}" @endif >
                     </div>
                 
                     <div class="form-group">
                        <lable>Marital Status </lable>
                        <select name="marital_status" id="marital_status" class=" form-control" >
                           <option value ="">Marital Status </option>
                           <option value="Single" @if(!empty($datingProfile['marital_status']) && $datingProfile['marital_status']=="Single") selected="" @endif>Single</option>
                           <option value="Married"  @if(!empty($datingProfile['marital_status']) && $datingProfile['marital_status']=="Married") selected="" @endif>Married</option>
                           <option value="Widowed" @if(!empty($datingProfile['marital_status']) && $datingProfile['marital_status']=="Widowed") selected="" @endif>Widowed</option>
                           <option value="Separated" @if(!empty($datingProfile['marital_status']) && $datingProfile['marital_status']=="Separated") selected="" @endif>Separated</option>
                           <option value="Divorced" @if(!empty($datingProfile['marital_status']) && $datingProfile['marital_status']=="Divorced") selected="" @endif>Divorced</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <lable>Body Type </lable>
                        <select name="body_type" id="body_type" class=" form-control" >
                           <option value ="">Body Type </option>
                           <option value="Slim"@if(!empty($datingProfile['body_type']) && $datingProfile['body_type']=="Slim") selected="" @endif>Slim</option>
                           <option value="Average" @if(!empty($datingProfile['body_type']) && $datingProfile['body_type']=="Average") selected="" @endif>Average</option>
                           <option value="Athletic" @if(!empty($datingProfile['body_type']) && $datingProfile['body_type']=="Athletic") selected="" @endif>Athletic</option>
                           <option value="Heavy" @if(!empty($datingProfile['body_type']) && $datingProfile['body_type']=="Heavy") selected="" @endif>Heavy</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <lable>Complexion</lable>
                        <select name="complexion" id="complexion" class=" form-control" >
                           <option value ="">Complexion</option>
                           <option value="VeryFair" @if(!empty($datingProfile['complexion']) && $datingProfile['complexion']=="VeryFair") selected="" @endif>Very Fair</option>
                           <option value="Fair" @if(!empty($datingProfile['complexion']) && $datingProfile['complexion']=="Fair") selected="" @endif>Fair</option>
                           <option value="Wheatish"@if(!empty($datingProfile['complexion']) && $datingProfile['complexion']=="Wheatish") selected="" @endif>Wheatish</option>
                           <option value="Dark" @if(!empty($datingProfile['complexion']) && $datingProfile['complexion']=="Dark") selected="" @endif>Dark</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <lable>Gender</lable>
                        <select name="gender" id="gender" class=" form-control">
                           <option value ="0"selected>Gender</option>
                           <option value="male" @if($User->gender == "male")selected @endif> Male</option>
                           <option value="female" @if($User->gender == "female")selected @endif> Female</option>
                        </select>
                        <div id="errorInPopup" class="error displayInlineBlock"></div>
                     </div>
                     <div class="form-group">
                        <lable>Country</lable>
                        <select name="counrty" id="counrty" class=" form-control">
                           <option value ="" selected>Country</option>
                           @foreach($countries as $counrty)
                           <option value="{{$counrty->country_name}}" @if($counrty->country_name == $datingProfile['counrty'] ) selected @endif > {{$counrty->country_name}}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <lable>Language</lable>
                        <select name="language" id="language" class=" form-control">
                           <option value ="" selected>Language</option>
                           @foreach($languges as $languge)
                           <option value=" {{$languge->name}}" @if($languge->name == $datingProfile['language'] ) selected @endif > {{$languge->name}}</option>
                           @endforeach
                        </select>
                        <div id="errorInPopup" class="error displayInlineBlock"></div>
                     </div>
                     <div class="form-group">
                        <lable>Hobbies</lable>
                        <select name="hobby" id="hobby" class=" form-control">
                           <option  value =""selected>Hobbies</option>
                           @foreach($hobbies as $hobby)
                           <option value=" {{$hobby->title}}"  @if($hobby->title == $datingProfile['hobby'] ) selected @endif > {{$hobby->title}}</option>
                           @endforeach
                        </select>
                        <div id="errorInPopup" class="error displayInlineBlock"></div>
                     </div>
                     <button type="submit" id="btntest" value="add_dating" class="btn form-control login_btn"style="margin-bottom: 15px;">Add </button>
                  </div>
                  <div class="col-md-6">
                    <h3 style="text-align: center; margin-bottom: 12px;margin-top: 12px;"> Education & Career </h3>

                        <div class=" form-group" style="margin-bottom: 20px;">
                           <lable>Education</lable>
                            <input type="text" autocomplete="off" class="form-control" id="education" name="education"  @if(!empty($datingProfile['education'])) value="{{ $datingProfile['education'] }}" @endif placeholder="Highst Education ">
                        </div>
                        <div class=" form-group" style="margin-bottom: 20px;">
                           <lable>Occupation</lable>
                            <input type="text" autocomplete="off" class="form-control" id="occupation" name="occupation"   @if(!empty($datingProfile['occupation'])) value="{{ $datingProfile['occupation'] }}" @endif placeholder="Your Occupation : ">
                        </div>
                        <div class=" form-group" style="margin-bottom: 20px;">
                           <lable>Your Incom</lable>
                            <input type="text" autocomplete="off" class="form-control" id="income" name="income"  @if(!empty($datingProfile['income'])) value="{{ $datingProfile['income'] }}" @endif placeholder="Your Income : ">
                        </div>
                        <h3 style="text-align: center; margin-bottom: 12px;margin-top: 12px;"> About Myself </h3>
                            <div class="form-group shadow-textarea"> About Myself
                                <label for="exampleFormControlTextarea6"></label>
                                <textarea class="form-control z-depth-1" id="exampleFormControlTextarea6"  name="myself" rows="3" placeholder="Write something here...">@if(!empty($datingProfile['myself'])) {{ $datingProfile['myself'] }} @endif</textarea>
                            </div>
                            <div class="form-group shadow-textarea"> About My preferred partner
                                <label for="exampleFormControlTextarea6"></label>
                                <textarea class="form-control z-depth-1" id="exampleFormControlTextarea"  name="partner"  rows="3"  placeholder="Write something here...">@if(!empty($datingProfile['partner'])) {{ $datingProfile['partner'] }} @endif</textarea>
                            </div>
                  </div>

            </div>
            </form>
         </div>
      </div>
   </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
