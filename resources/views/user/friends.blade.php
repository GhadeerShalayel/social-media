<?php 
namespace App\Http\Controllers;
use Auth;
use App\User ;

?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area banner_area2">
   <div class="container">
      <div class="banner_content">
         <h3 title="My Frinds"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">My Frinds<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="">Community</a>
         <a href="blog.html">Frinds</a>
      </div>
   </div>
</section>

<!--================All Members Area =================-->
<section class="all_members_area">
   <div class="container">
      <div class="welcome_title">
         <h3 style="margin-top: 12px;"><span><i class="fa fa-heart"></i></span>   My Frinds  <span><i class="fa fa-heart	"></i></span></h3>
         <img src="img/w-title-b.png" alt="">
      </div>
     
      <div class="row">
         @foreach($frinds as $frind)

         <?php
               //هان بيانات الاصدقاء الي هما بعتولي طلب
               $frind_name = User::getName($frind->friend_id);    
               $frind_image = User::getImage($frind->friend_id);  
            
         ?>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="{{ asset ('images/frontend_images/photos/'.$frind_image)}}" alt="">
               <h4> <a style="color:black;" href = "{{url('/profile/'.$frind_name)}}" >{{$frind_name}}</a></h4>
               <div class="hover_icon">
                  <ul>
                     <li><a><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
         @endforeach
      </div>
      {{ $frinds->links( ) }}

   </div>
</section>
<!--================End All Members Area =================-->
@endsection
