<?php use App\User ;
      use App\Responses;
?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<style>
.seenResponse{
    font-weight: normal !important;
}
</style>
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Dating Profile<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">Dating Profile</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
      <div class="register_form_inner zoom-anim-dialog " id="register_form">
         <div class="row">
            @if(Session::has('flash_message_error'))    
            <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_error') !!} </strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))  
            <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_success') !!} </strong>
            </div>
            @endif
            <div class="registration_form_s ">
               <h4>Frind Request</h4>
               <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                     <tr>
                        <th style="text-align : center;">Name</th>
                        <th style="text-align : center;"> Date /Time </th>
                        <th style="text-align : center;">Actions</th>

                     </tr>
                  </thead>
                  <tbody>
                    @foreach($frindRequest as $frind)
                        <?php  $sender_name = User::getName($frind->user_id);
                            
                        ?>
                     <tr>
                        <td style="text-align : center;">  <a style="color:black;" href = "{{url('/profile/'.$sender_name)}}" >{{$sender_name}}</a></td>
                        <td style="text-align : center;" ></span>{{$frind->created_at}}</td>

                        <td  style="text-align:center;">
                           <a  href="{{url('/accept-friend-request/'.$frind->user_id)}}"   style="color:black;" href=""> <i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></i></a>                                
                           <a href="{{url('/reject-friend-request/'.$frind->user_id)}}" style="color:black;" href=""> <i class="fa fa-times-circle fa-lg" aria-hidden="true"></i></i></a>                                

                        </td>

                     </tr>
                     @endforeach
                  </tbody>
               </table>                                                         
            </div>
         </div>
      </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
