@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Register<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">Register</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
   <div class="register_form_inner zoom-anim-dialog " id="register_form">
            <div class="row">
                <div class="col-md-6">
                    <div class="registration_man">
                        <img src="{{ asset ('images/frontend_images/registration-man1.png')}}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="registration_form_s ">
                        @if(Session::has('flash_message_error'))    
              
                            <div class="alert alert-warning alert-block alert_message1">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                        @endif

                        @if(Session::has('flash_message_success'))  
                            <div class="alert alert-success alert-block alert_message1">
                                <button type="button" class="close" data-dismiss="alert">×</button>	
                                <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                        @endif
                        <h4>Registration</h4>
                        <form method="post" action="{{url('/register')}}" id="registerForm">{{csrf_field()}}

                            <div class="form-group">
                                <input type="text" class="form-control" id="username" name="username" placeholder="User Name">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="reg_email" name="reg_email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="reg_pass" name="reg_pass" placeholder="Password">
                                <span id="passstrength"></span>

                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="conf_pass" name="conf_pass" placeholder="Confirm Password">
                            </div>

                            <div class="form-group">
                                    <select name="gender" id="gender" class=" form-control" id="">
                                        <option value ="0"selected>Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    <div id="errorInPopup" class="error displayInlineBlock"></div>

                            </div>

                            <div class="reg_chose form-group">
                                <div class="reg_check_box" style="margin-bottom: 30px;">
                                    <label for="s-option">Please agree to our policy</label>
                                    <input type="radio" id="s-option" name="selector">
                                    <div class="check"><div class="inside"></div></div>
                                </div>

                                <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                

                                <div class="col-md-6 pull-center">
                                    {!! app('captcha')->display() !!}

                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                                <button type="submit" id="btntest" value="LogIn" class="btn form-control login_btn"style="margin-top: 80px;">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
