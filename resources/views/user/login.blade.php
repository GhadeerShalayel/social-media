@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Log In"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Login<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">log in</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
      <div class="login_form_inner zoom-anim-dialog " id="small-dialog">
         <form method="post" action="{{url('/login')}}" id="loginForm" style="margin-top: 20px;">
            {{csrf_field()}}
            @if(Session::has('flash_message_error'))    
            <div class="alert alert-danger alert-block alert_message ">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_error') !!} </strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))  
            <div class="alert alert-success alert-block alert_message">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_success') !!} </strong>
            </div>
            @endif
            <input type="text" name="username" placeholder="Username">
            <input type="password" name="password" placeholder="Password">
            <div class="login_btn_area">
               <button type="submit" value="LogIn" name="login" class="btn form-control login_btn">LogIn</button>
               <div class="login_social">
                  <h5>Login With</h5>
                  <ul>
                     <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
               </div>
            </div>
         </form>
      </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
