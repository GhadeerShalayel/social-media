@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="login_form_inner zoom-anim-dialog mfp-hide" id="small-dialog">
    <?php $user_name = Auth::user()['user_name'] ; ?>
           <form id ="messageForm "action="{{url('/contact/'.$userDetails['user_name'])}}" method="post">
               {{csrf_field()}}
                <div class="form-group shadow-textarea"> 
                    <label for="exampleFormControlTextarea6"></label>
                    <textarea  class="form-control z-depth-1" id="exampleFormControlTextarea6" id="message" name="message" rows="3" required placeholder="Write Your Message here..."></textarea>
                    <input type="hidden" name="receiver_id" value="{{$userDetails['id']}}">
                    <input type="hidden" name="sender_id" value="{{Auth::User()['id']}}">
                </div> 
                <div class="login_btn_area">
                   <button type="submit" value="LogIn" class="btn form-control login_btn" style="width: 90px; text-align: center; " >Send</button>
               </div>
           </form>
           <img class="mfp-close" src="img/close-btn.png" alt="">
        </div>

       <!--================Banner Area =================-->
       <section class="banner_area profile_banner">
            <div class="profiles_inners">
                <div class="container">
                    <div class="profile_content">
                        <div class="user_img">
                        @if(!empty($userDetails['photos']))
                                @foreach($userDetails['photos'] as $key => $photo)
                                    @if($photo['default_photo'] == "Yes")
                                        <?php $user_photo = $userDetails['photos'][$key]['photo'] ;?>
                                    @else
                                        <?php $user_photo = $userDetails['photos'][0]['photo'] ; ?>
                                    @endif
                                @endforeach

                                @if(!empty($user_photo))
                                <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/'.$user_photo)}}" alt="">
                                @endif
                        @else  
                                @if($userDetails['gender']=="male")
                                        <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/male.png')}}" alt="">
                                @else
                                        <img class="img-circle" style="width:270px; hieght:270px" src="{{ asset ('images/frontend_images/photos/female.jpg')}}" alt="">

                                @endif

                        @endif
                        </div>
                        <div class="user_name">
                            <h3>{{ $userDetails['user_name']}}</h3>
                            <h4> <?php echo $diff = date('Y') - date('Y',strtotime($userDetails['details']['dob']));?> years old</h4>
                            <ul>
                                <li><a>{{$userDetails['details']['counrty']}}, {{$userDetails['details']['city']}}</a></li>
                            
                            </ul>
                        </div>
                        <div class="right_side_content">
                            @if(Auth::User()['id'] && ($userDetails['id'] != Auth::User()['id']))

                                @if(!empty($frindRequest))
                                    @if($frindRequest == "Add Frind")
                                        <button onclick="location.href='{{ url('/add-friend/'.$userDetails['user_name']) }}'" value="LogIn" class="btn form-control login_btn" >{{$frindRequest}}<img src="{{ asset ('images/frontend_images/user.png')}}" alt=""></button>
                                    @elseif($frindRequest == "Unfrind")
                                    <button onclick="location.href='{{ url('/remove-friend/'.$userDetails['user_name']) }}'" value="LogIn" class="btn form-control login_btn" >{{$frindRequest}}</button>
                                    @elseif($frindRequest == "Request Sent")
                                        <button value="LogIn" class="btn form-control login_btn" >{{$frindRequest}}</button>

                                    @endif
                                @endif
                            <button href="#small-dialog" type="submit" value="LogIn" class="btn form-control login_btn popup-with-zoom-anim">Chat Now <img src="{{ asset ('images/frontend_images/comment.png')}}" alt=""></button>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Banner Area =================-->
        
        
        <!--================Blog grid Area =================-->
        <section class="blog_grid_area">
            <div class="container">
                <div class="row">
                @if(Session::has('flash_message_error'))    
                <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif
                @if(Session::has('flash_message_success'))  
                <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong> {!! session('flash_message_success') !!} </strong>
                </div>
                @endif
                    <div class="col-md-9">
                        <div class="members_profile_inners">
                            <ul class="nav nav-tabs profile_menu" role="tablist">
                                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                                <li role="presentation"><a href="#friend" aria-controls="friend" role="tab" data-toggle="tab">Friend (260)</a></li>
                               
                            </ul>
                            <div class="tab-content">
                               
                                <div role="tabpanel" class="tab-pane active fade in" id="profile">
                                    <div class="profile_list">
                                        <ul>
                                            <li><a >Gender</a></li>
                                            <li><a >Age</a></li>
                                            <li><a >Country</a></li>
                                            <li><a >City</a></li>
                                            <li><a >Birthday</a></li>
                                            <li><a >Relationship</a></li>
                                            <li><a >Looking for a</a></li>
                                            <li><a >Work as</a></li>
                                            <li><a>Income</a></li>

                                        </ul>
                                        <ul>
                                            <li><a>{{$userDetails['gender']}}</a></li>
                                            <li><a><?php echo $diff = date('Y') - date('Y',strtotime($userDetails['details']['dob']));?> years old</a></li>
                                            <li><a>{{$userDetails['details']['counrty']}}</a></li>
                                            <li><a>{{$userDetails['details']['city']}}</a></li>
                                            <li><a>{{$userDetails['details']['dob']}}</a></li>
                                            <li><a>Single</a></li>
                                            <li><a>Man</a></li>
                                            <li><a>{{$userDetails['details']['occupation']}}</a></li>
                                            <li><a>{{$userDetails['details']['income']}}</a></li>

                                        </ul>
                                        <ul>
                                            <li><a>Education</a></li>
                                            <li><a>Language</a></li>
                                            <li><a>Hobby</a></li>
                                            <li><a>Weight</a></li>
                                            <li><a>Height</a></li>
                                            <li><a>Eye Color</a></li>
                                            <li><a>Marital Status</a></li>
                                            <li><a>Looking for a</a></li>
                                        </ul>
                                        <ul>
                                            <li><a>{{$userDetails['details']['education']}}</a></li>
                                            <li><a>{{$userDetails['details']['language']}}</a></li>
                                            <li><a>{{$userDetails['details']['hobby']}}</a></li>
                                            <li><a>{{$userDetails['details']['weight']}}</a></li>
                                            <li><a>{{$userDetails['details']['height']}}</a></li>
                                            <li><a>Black</a></li>
                                            <li><a>{{$userDetails['details']['marital_status']}}</a></li>
                                            <li><a>Man</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade" id="friend">
                                    <div class="profile_list">
                                  
                                    </div>
                                </div>
                               
                              
                            </div>
                            <div class="members_about_box">
                                <h4>About me</h4>
                                <p>{{$userDetails['details']['myself']}}.</p>
                            </div>
                            <div class="members_about_box">
                                <h4>Looking For</h4>
                                <p>{{$userDetails['details']['partner']}}.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="right_sidebar_area">
                            <aside class="s_widget photo_widget">
                                <div class="s_title">
                                    <h4>Photo</h4>
                                    <img src="{{ asset ('images/frontend_images/widget-title-border.png')}}" alt="">
                                </div>
                                <ul>
                                    @foreach( array_slice($userDetails['photos'] ,0 ,9) as $key => $photo)
                                        @if($photo['status']==1)
                                            <li><a ><img src="{{url('images/frontend_images/photos/'.$photo['photo'] )}}" alt=""></a></li>
                                        @endif
                                    @endforeach
                                    @if($photo['status']==1)
                                        <li><a href="{{ url('/profile/photo/'.$userDetails['user_name'] )}}">Show all Photo</a></li>
                                    @endif


                                </ul>

                            </aside>
                            <aside class="s_widget recent_post_widget">
                                <div class="s_title">
                                    <h4>Recent Post</h4>
                                    <img src="img/widget-title-border.png" alt="">
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="img/blog/recent-post/recent-post-1.jpg" alt="">
                                    </div>
                                    <div class="media-body">
                                        <h4>Blog Image Post</h4>
                                        <a href="#">14 Sep, 2016 at 08:00 Pm</a>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="img/blog/recent-post/recent-post-2.jpg" alt="">
                                    </div>
                                    <div class="media-body">
                                        <h4>Blog Standard Post</h4>
                                        <a href="#">14 Sep, 2016 at 08:00 Pm</a>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="img/blog/recent-post/recent-post-3.jpg" alt="">
                                    </div>
                                    <div class="media-body">
                                        <h4>Blog Image Post</h4>
                                        <a href="#">14 Sep, 2016 at 08:00 Pm</a>
                                    </div>
                                </div>
                            </aside>
                            <aside class="s_widget social_widget">
                                <div class="s_title">
                                    <h4>Tags</h4>
                                    <img src="img/widget-title-border.png" alt="">
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Blog grid Area =================-->
        
    
@endsection