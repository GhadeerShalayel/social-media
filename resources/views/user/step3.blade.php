<?php use App\User;
    $DatingProfileCount = User::datingProfileExists(Auth::User()['id']);
    if($DatingProfileCount ==1 ){
        $datingProfileText = "My Dating Profile";
    }else{
       $datingProfileText = "Add Dating Profile";
    }
    $datingProfile = User::datingProfileDetails(Auth::User()['id']);
    //die;
    ?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_content">
            <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Dating Profile<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
            <a href="index.html">Home</a>
            <a href="#">Pages</a>
            <a href="why-us.html">Dating Profile</a>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
    <div class="container">
        <div class=" register_form_inner zoom-anim-dialog " id="register_form">
            <div class="row">
                @if(Session::has('flash_message_error'))    
                <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif
                @if(Session::has('flash_message_success'))  
                <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
                    <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong> {!! session('flash_message_success') !!} </strong>
                </div>
                @endif
                <div class="registration_form_s ">
                    <h4>My Photos</h4>
                    <h5 style="text-align :center;">You can Upload multiple photo of your choice .</h5>
                    <form method="post" action="{{url('/step/3')}}" id="PhotoForm" name="PhotoForm" enctype="multipart/form-data" >
                    {{csrf_field()}} 
                        <input type="hidden" name="user_id" vale="{{Auth::User()['id']}}">
                        <div class="form-group row" style="margin-top: 50px; margin-bottom: 50px;">
                            <label for="Photo" class="col-sm-2 col-form-label">Your Photo : *</label>
                            <div class="col-sm-10">
                            <input autocomplete="off" id="photo" name="photo[]" type="file" multiple="multiple"  style="border-top-width: 10px;" />
                            </div>
                        </div>
                        <button type="submit" id="btntest" value="add_dating" class="btn form-control login_btn"style="margin-bottom: 15px;">Add </button>

                    </form>
                    
                </div>         
        </div>
    </div>

   <div class="container">
                <div class="row">
                    @foreach($user_photos as $photo)
                    <div class="col-md-4 col-sm-6">
                        <div class="product_item">
                            <div class="product_img">
                                <img style="hieght:300px ; width:300px;" src="{{url('images/frontend_images/photos/'.$photo['photo'])}}" alt="">
                                    <ul>
                                    @if($photo['default_photo'] == "Yes")

                                    <li><a ></i> (Defult)</a></li>
                                    
                                    @endif
                                    </ul>
                                <div class="hover_icon">
                                    <ul>
                                        <li><a rel="{{ $photo->photo }}" rel1="delete-photo"  href="javascript:" class="deleteAction"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                        <li><a  href="{{url('/default-photo/'.$photo->photo )}}" ></i>Defult</a></li>

                                    </ul>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    @endforeach
                </div>
    </div>
    
</section>


@endsection


