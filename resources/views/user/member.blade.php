<?php 
namespace App\Http\Controllers;
use Auth;
use App\User ;

?>
@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area banner_area2">
   <div class="container">
      <div class="banner_content">
         <h3 title="Members"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Members<img class="right_img" src="img/banner/t-right-img.png" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="">Community</a>
         <a href="blog.html">Members</a>
         <div class="advanced_search">
            <form action="{{url('/search')}}" method="post">
               {{@csrf_field()}} 
            <div class="search_inner">
               <div class="search_item">
                   <?php    $gender = Auth::User()['gender'];?>
                   <input type="hidden" value="{{ $gender }}">
                  <h5>Seeking a</h5>
                  <select class="selectpicker" name="gender">
                     <option value="female">Woman</option>
                     <option value="male">Man</option>
                  </select>
               </div>
               <div class="search_item">
                  <h5>From</h5>
                  <select class="selectpicker" name="minAge">
                    <?php   $minacount = 16 ;
                    while($minacount <= 99 ){ ?>
                     <option value="{{$minacount}}">{{$minacount}}</option>
                     <?php $minacount = $minacount + 1 ; } ?>
                  </select>
               </div>
               <div class="search_item">
                  <h5>To</h5>
                  <select class="selectpicker" name="maxAge">
                    <?php   $maxacount = 16 ;
                    while($maxacount <= 99 ){ ?>
                     <option value="{{$maxacount}}">{{$maxacount}}</option>
                     <?php $maxacount = $maxacount + 1 ; } ?>

                  </select>
               </div>
               <div class="search_item">
                  <h5>Location</h5>
                  <select class="selectpicker" name="location">
                  <option value ="" selected>AnyWhere</option>
                        @foreach($countries as $counrty)
                        <option value="{{$counrty->country_name}}" @if($counrty->country_name == "palestine") selected @endif  > {{$counrty->country_name}}</option>
                        @endforeach
                  </select>
               </div>
               <div class="search_item">
                  <button type="submit" style="width: 130px;" value="LogIn" class="btn form-control login_btn">Search</button>
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Active Memebers Area =================-->

<!--================End Active Memebers Area =================-->
<!--================Register Members slider Area =================-->
<section class="register_members_slider">
   <div class="container">
      <div class="welcome_title">
         <h3>Latest registered members</h3>
         <img src="img/w-title-b.png" alt="">
      </div>
      <div class="r_members_inner">
      @foreach($recent_users as $user)
      @if(!empty($user['details']) && $user['details']['status']==1 )
      <div class="item">
         @if(!empty($user['photos']))
         @foreach($user['photos'] as $key => $photo)
         @if($photo['default_photo'] == "Yes")
         <?php $user_photo = $user['photos'][$key]['photo'] ;?>
         @else
         <?php $user_photo = $user['photos'][0]['photo'] ; ?>
         @endif
         @endforeach
         @if(!empty($user_photo))
         <img  class="img-circle"   src="{{ asset ('images/frontend_images/photos/'.$user_photo)}}" alt="">
         @endif
         @else
         @if($user['gender']=="male")
         <img class="img-circle"   src="{{ asset ('images/frontend_images/photos/male.png')}}" alt="">
         @else
         <img class="img-circle"   src="{{ asset ('images/frontend_images/photos/female.jpg')}}" alt="">
         @endif
         @endif
         <h4 ><a style="color:black" href="{{url('/profile/'.$user['user_name'])}}" >{{$user['user_name']}}</a></h4>
         <h5>
         <?php echo $diff = date('Y') - date('Y',strtotime($user['details']['dob']));?> Year </5>
      </div>
      @endif
      @endforeach
      </div>
   </div>
</section>
<!--================End Register Members  slider Area =================-->
<!--================All Members Area =================-->
<section class="all_members_area">
   <div class="container">
      <div class="welcome_title">
         <h3>All Members</h3>
         <img src="img/w-title-b.png" alt="">
      </div>
      <div class="row">
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-1.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-2.png" alt="">
               <h4>Alex Jones</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-3.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-4.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-5.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-6.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-7.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-8.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-9.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-10.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-11.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
         <div class="col-sm-2 col-xs-6">
            <div class="all_members_item">
               <img src="img/members/all-members/members-12.png" alt="">
               <h4>Lena Adms</h4>
               <h5>22 years old</h5>
            </div>
         </div>
      </div>
      <a href="#" class="register_angkar_btn">View More</a>
   </div>
</section>
<!--================End All Members Area =================-->
@endsection
