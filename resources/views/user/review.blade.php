@extends('layouts.frontlayouts.front_design')
@section('content')
<!--================Banner Area =================-->
<section class="banner_area">
   <div class="container">
      <div class="banner_content">
         <h3 title="Register"><img class="left_img" src="{{ asset ('images/frontend_images/banner/t-left-img.png')}}" alt="">Dating Profile<img class="right_img" src="{{ asset ('images/frontend_images/banner/t-right-img.png')}}" alt=""></h3>
         <a href="index.html">Home</a>
         <a href="#">Pages</a>
         <a href="why-us.html">Dating Profile</a>
      </div>
   </div>
</section>
<!--================End Banner Area =================-->
<!--================Find Your Soul Area =================-->
<section class="find_soul_area">
   <div class="container">
      <div class="register_form_inner zoom-anim-dialog " id="register_form">
         <div class="row">
            @if(Session::has('flash_message_error'))    
            <div class="alert alert-warning alert-block alert_message1" style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_error') !!} </strong>
            </div>
            @endif
            @if(Session::has('flash_message_success'))  
            <div class="alert alert-success alert-block alert_message1 " style="margin-top: 12px;">
               <button type="button" class="close" data-dismiss="alert">×</button>	
               <strong> {!! session('flash_message_success') !!} </strong>
            </div>
            @endif
            <div class="registration_form_s ">
               <h4>Your profile under review </h4>
               <div style="text-align: center ;margin-bottom: 30px;margin-top: 30px;">   
                    as part of the process, we screen every profile to ensure whether it meets our terms and condition. you will receive an email from us on your profile status as soon as the screening is complete. 
               </div>
         </div>
      </div>
   </div>
   </div>
</section>
<!--================End Find Your Soul Area =================-->
@endsection
