<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{ asset ('images/frontend_images/fav-icon.png')}}" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>VeroDate - Dating Social Network Website</title>

        <!-- Icon css link -->
        <link href="{{ asset ('css/frontend_css/materialdesignicons.min.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/linears-icon/style.css')}}" rel="stylesheet">
        
        <!-- RS5.0 Layers and Navigation Styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset ('css/frontend_css/layers.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset ('css/frontend_css/navigation.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset ('css/frontend_css/settings.css')}}">
        
        <!-- Bootstrap -->

        <link href="{{ asset ('css/frontend_css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <link href="{{ asset ('css/frontend_css/dd.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/magnific-popup.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/bootstrap-select.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/animate.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/jquery.webui-popover.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/jquery-ui.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" />
        <link href="{{ asset ('css/frontend_css/style.css')}}" rel="stylesheet">
        <link href="{{ asset ('css/frontend_css/responsive.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      
       
            @include('layouts.frontlayouts.front_header')
            @yield('content')
            @include('layouts.frontlayouts.front_footer')

        
        
       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>

       <script src="{{ asset ('js/frontend_js/map-custome.js')}}"></script>
       <script src="{{ asset ('js/frontend_js/jquery-3.1.1.js')}}"></script>
       <script src="{{ asset ('js/frontend_js/jquery.js')}}"></script>
       <script src="{{ asset ('js/frontend_js/jquery.validate.js')}}"></script>
       <script src="{{ asset ('js/frontend_js/additional-methods.min.js')}}"></script>
       <script src="{{ asset ('js/frontend_js/main.js')}}"></script>
       <script src="https://www.google.com/recaptcha/api.js" async defer></script>

   
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      
        <script src="{{ asset ('js/frontend_js/jquery-2.1.4.min.js')}}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <script src="{{ asset ('js/frontend_js/bootstrap.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery.themepunch.revolution.min.js')}}"></script>
        <!--RS5.0 Extensions-->
        <script src="{{ asset ('js/frontend_js/jquery.validate.js') }}"></script> 

        <script src="{{ asset ('js/frontend_js/revolution.extension.actions.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/revolution.extension.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset ('js/frontend_js/revolution.extension.kenburn.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset ('js/frontend_js/revolution.extension.layeranimation.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset ('js/frontend_js/revolution.extension.migration.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/revolution.extension.navigation.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/revolution.extension.parallax.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/revolution.extension.video.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
        <script src="{{ asset ('js/frontend_js/lg-thumbnail.min.js') }}"></script>
        <script src="{{ asset ('js/frontend_js/lg-fullscreen.min.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <!-- Extra plugin js -->
        <script src="{{ asset ('js/frontend_js/jquery.dd.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/wow.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/bootstrap-select.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/moment-with-locales.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/waypoints.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery.counterup.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery.webui-popover.min.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/jquery-ui.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/video_player.js')}}"></script>
        <script src="{{ asset ('js/frontend_js/theme.js')}}"></script>
        <script>
            $(function() {
                $( "#dob" ).datepicker({ 
                    dateFormat: 'yy-mm-dd',
                    changeMonth:true,
                    changeYear:true,
                    maxDate: '0',
                    yearRange: '1950:2020'

                 });
            });
        </script>
   
    </body>
</html>