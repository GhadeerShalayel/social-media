  <?php use App\User;
     $DatingProfileCount = User::datingProfileExists(Auth::User()['id']);
     if($DatingProfileCount ==1 ){
         $datingProfileText = "My Dating Profile";
     }else{
        $datingProfileText = "Add Dating Profile";

     }
  //die;
  use App\Responses;
  ?>
 <!--================Frist Main hader Area =================-->
 <header class="header_menu_area">
            <nav class="navbar navbar-default">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="{{ asset ('images/frontend_images/logo.png')}}" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown submenu active">
                            <a href="{{url('/')}}"  role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                        </li>
                        <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('/members')}}">Members</a></li>
                                <li><a href="{{url('/search')}}">Search</a></li>
                                @if((Auth::check()))
                                <?php $username = Auth::User()['user_name'] ; ?>
                                <li><a href="{{url('/profile/'.$username)}}">My Profile</a></li>

                                <li><a href="{{url('/responses')}}">Messages Received(<span class="newResponsesCount" >{{Responses::newResponsesCount()}})</span> </a></li>
                                <li><a href="{{url('/sent-messages')}}">Messages Sent</a></li>
                                <li><a href="{{url('/friends')}}">My Frinds</a></li>
                                <li><a href="{{url('/friends-requests')}}">Frinds Request</a></li>


                          
                                @endif

                            </ul>
                        </li>
                        <li><a href="contact.html">Contact us</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if(empty(Auth::check()))
                        <li><a class="" href="{{url('/login')}}"><i class="mdi mdi-key-variant"></i>Login</a></li>
                        <li><a href="{{url('/register')}}" class=""><i class="fa fa-user-plus"></i>Registration</a></li>
                        @else
                        <li><a href="{{url('/step/2')}}" class=""><i class="fa fa-user-plus"></i>{{$datingProfileText}}</a></li>
                        @if($DatingProfileCount ==1 )
                            <li><a href="{{url('/step/3')}}" class=""><i class="fa fa-image"></i></i>My Photo</a></li>
                        @endif
                        <li><a href="{{url('/logout')}}" class=""><i class="mdi mdi-key-variant"></i>Log Out</a></li>

                        @endif
                        
                    </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        <!--================Frist Main hader Area =================-->
  