<?php $url = url()->current(); ?>
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Members</span> <span class="label label-important">1</span></a>
      <ul <?php if (preg_match("/members/i", $url)){ ?> style="display: block;" <?php } ?>>
        <li <?php if (preg_match("/view-members/i", $url)){ ?> class="active" <?php } ?>><a href="{{ url('/admin/view-users')}}">View Users</a></li>
      </ul>
    </li>


  </ul>
</div>
<!--sidebar-menu-->
